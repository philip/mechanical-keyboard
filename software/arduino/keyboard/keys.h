#pragma once
// Special keys: https://www.arduino.cc/en/Reference/KeyboardModifiers

enum Keys {
    LEFT_CTRL = 128,
    LEFT_SHIFT = 129,
    LEFT_ALT = 130,
    LEFT_GUI = 131,
    RIGHT_CTRL = 132,
    RIGHT_SHIFT = 133,
    RIGHT_ALT = 134,
    RIGHT_GUI = 135,
    UP_ARROW = 218,
    DOWN_ARROW = 217,
    LEFT_ARROW = 216,
    RIGHT_ARROW = 215,
    BACKSPACE = 178,
    TAB = 179,
    RETURN = 176,
    ESC = 177,
    INSERT = 209,
    DELETE = 212,
    PAGE_UP = 211,
    PAGE_DOWN = 214,
    HOME = 210,
    END = 213,
    CAPS_LOCK = 193,
    F1 = 194,
    F2 = 195,
    F3 = 196,
    F4 = 197,
    F5 = 198,
    F6 = 199,
    F7 = 200,
    F8 = 201,
    F9 = 202,
    F10 = 203,
    F11 = 204,
    F12 = 205,
};
