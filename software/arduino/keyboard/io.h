/*
 * io.h
 *
 *  Created on: 3 mars 2020
 *      Author: Mattias Larsson Sköld
 */

#pragma once

#include "keyboardmap.h"
#include "keyboardstate.h"
#include "pins.h"

// Standad layer
const char *keyConfig1 = "\6912345" // Fix this. Put in the right scancodes
                         "\tQWERT"
                         "\177ASDFG"
                         "\129<ZXCV"
                         "\128\131\130   ";

typedef KeyboardState<width, height> keyboard_state_t;
keyboard_state_t state;

KeyboardMap keyboardMap(width, height, keyConfig1);

void setup() {
    for (auto pin : xPins) {
        pinMode(pin, OUTPUT);
    }
}

//! Turn on one column pin and turn of the rest
void changeColumnPin(size_t column) {
    for (size_t i = 0; i < width; ++i) {
        digitalWrite(xPins[i], i == column);
    }
}

void readRowPins(keyboard_state_t &keyboardState, size_t x) {
    for (size_t y = 0; y < height; ++y) {
        int value = digitalRead(yPins[y]);
        auto &storedKeyState = keyboardState.state(x, y);

        if (storedKeyState != value) {
            Serial.print("readRowPins: key is changed ");
            Serial.print(x);
            Serial.print(y);
            keyboardMap.publishEvent(x, y, value);
            storedKeyState = value;
        }
    }
}

void loop() {
    // Cycle through columns
    for (size_t x = 0; x < width; ++x) {
        changeColumnPin(x);

        readRowPins(state, x);
    }
}
