/*
 * pins.h
 *
 *  Created on: 3 mars 2020
 *      Author: Mattias Larsson Sköld
 */

#pragma once

constexpr size_t width = 6;
constexpr size_t height = 5;

const char xPins[width] = {1, 2, 3, 4, 5, 6};
const char yPins[height] = {8, 9, 10, 11, 12};
