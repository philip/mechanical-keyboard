#pragma once

template <size_t width, size_t height>
class KeyboardState {
    char _state[width * height] = {};

public:
    typedef char state_t;

    // Todo: Bounds check
    state_t state(size_t x, size_t y) const {
        return _state[y * width + x];
    }
    state_t &state(size_t x, size_t y) {
        return _state[y * width + x];
    }
};
