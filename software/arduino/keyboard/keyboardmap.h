
#pragma once

#include "keyboardwritefunctions.h"
#include "keys.h"

//! A class for translating key coordinates to scancodes
class KeyboardMap {
    size_t _width;
    size_t _height;
    size_t _size;
    const char *_translation;

public:
    KeyboardMap(size_t width, size_t height, const char *values)
        : _width(width), _height(height), _size(width * height),
          _translation(values) {
    }

    void publishEvent(size_t x, size_t y, bool keyValue) {
        size_t index = x + y * _width;
        if (index < _size) {
            auto out = _translation[index];
            if (keyValue) {
                writeKeyPress(out);
            }
            else {
                writeKeyRelease(out);
            }
        }
    }
};
