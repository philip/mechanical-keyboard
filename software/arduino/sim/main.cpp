/*
 * main.cpp
 *
 *  Created on: 3 mars 2020
 *      Author: Mattias Larsson Sköld
 */

#include "keys.h"

#include "matgui/application.h"
#include "matgui/button.h"
#include "matgui/window.h"

// Simulator
#include "keyboardwritefunctions.h"
#include "simio.h"

// The code
#include "io.h"

#include <iostream>

using namespace std;
using namespace MatGui;

std::string scanCodeLookup(int code) {
    return Application::GetKeyNameFromScancode(code);
}

int main(int argc, char **argv) {
    Application app(argc, argv);

    Window window("keyboard simulator");

    setup();

    for (size_t y = 0; y < height; ++y) {
        auto layout = make_unique<LinearLayout>();
        layout->orientation(LayoutOrientation::LAYOUT_HORIZONTAL);
        for (size_t x = 0; x < width; ++x) {
            auto button = make_unique<Button>("hej");
            button->pointerDown.connect([x, y](Button::PointerArgument arg) {
                cout << "clicked " << x << ", " << y << endl;
                simio::setSimKey(x, y, true);
            });
            button->pointerUp.connect([x, y](Button::PointerArgument arg) {
                cout << "main.cpp: released " << x << ", " << y << endl;
                simio::setSimKey(x, y, false);
            });
            layout->addChild(std::move(button));
        }
        window.addChild(std::move(layout));
    }

    window.frameUpdate.connect([]() { loop(); });

    app.mainLoop();
}
