/*
 * keyboard.cpp
 *
 *  Created on: 3 mars 2020
 *      Author: Mattias Larsson Sköld
 */

#include <iostream>
using namespace std;

std::string scanCodeLookup(int code); // Defined in main

void writeKeyRelease(char key) {
    cout << "Key released " << key << endl;
}

void writeKeyPress(char key) {
    cout << "keyboard.cpp: key pressed " << key << ", "
         << static_cast<unsigned>(key) << endl;
    cout << "that is: " << scanCodeLookup(key) << endl;
}
