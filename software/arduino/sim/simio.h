/*
 * simio.h
 *
 *  Created on: 3 mars 2020
 *      Author: Mattias Larsson Sköld
 */

#pragma once

#include <iostream>

#define HIGH 0x1
#define LOW 0x0

#define INPUT 0x0
#define OUTPUT 0x1
#define INPUT_PULLUP 0x2

#define PI 3.1415926535897932384626433832795
#define HALF_PI 1.5707963267948966192313216916398
#define TWO_PI 6.283185307179586476925286766559
#define DEG_TO_RAD 0.017453292519943295769236907684886
#define RAD_TO_DEG 57.295779513082320876798154814105
#define EULER 2.718281828459045235360287471352

static class SerialPort {
public:
    template <class T>
    void print(const T &value);

} Serial;

void digitalWrite(uint8_t pin, uint8_t value);
int digitalRead(uint8_t pin);
void pinMode(uint8_t pin, uint8_t mode);

template <class T>
inline void SerialPort::print(const T &value) {
    std::cout << value << std::endl;
}

namespace simio {

void setSimKey(size_t x, size_t y, uint8_t state);

}
