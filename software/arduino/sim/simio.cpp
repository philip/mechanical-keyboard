/*
 * simio.cpp
 *
 *  Created on: 3 mars 2020
 *      Author: Mattias Larsson Sköld
 */

#include "simio.h"
#include <array>
#include "pins.h"
#include <algorithm>

using namespace simio;

namespace {

std::array<uint8_t, 255> pinState = {};
std::array<uint8_t, width * height> simKeyState = {};

auto getSimKey(size_t x, size_t y) {
    return simKeyState.at(x + y * width);
}

} // namespace

void digitalWrite(uint8_t pin, uint8_t value) {
    pinState.at(pin) = value;
}


int digitalRead(uint8_t ypin) {
    auto found = std::find(yPins, yPins + height, ypin);
    if (found == yPins + height) {
        throw "not a valid input pin";
    }
    size_t y = found - yPins;
    for (int x = 0; x < width; ++x) {
        if (getSimKey(x, y) * pinState.at(xPins[x])) {
            return true;
        }
    }

    return false;
}

void pinMode(uint8_t pin, uint8_t mode) {
    // Does not make so much sense with
}

namespace simio {

void setSimKey(size_t x, size_t y, uint8_t state) {
    simKeyState.at(x + y * width) = state;
}

}



