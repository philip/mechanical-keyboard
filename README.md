# Mechanical Keyboard
This is Philip and Mattias awesome project for the universally best keyboard of all mother of keyboards both HaRdWaRe and of course **SOFTWARE!!!** For this, we begin with version 2.0 (obs. patch 234 before we have even written any code)

## KiCad Paths
In KiCad we use a path variable to gather all used components and footprints. To set this variable (if using KiCad version >5) go to Preferences->Configure Paths and add

| Name            | path                                                    |
| --------------- | ------------------------------------------------------- |
| KEYBOARD_PROJ_1 | **\<path to project>**\mechanical-keyboard\hardware\lib |